set PATH=.;d:\compiler\bin;d:\compiler\gcc-4.6.3\bin;C:\Program Files (x86)\ImageMagick-6.8.1-Q16;%SystemRoot%\system32;%SystemRoot%;%SystemRoot%\System32\Wbem;D:\compiler\texmf\miktex\bin;d:\compiler\perl-basic\bin;C:\Program Files (x86)\gs\gs9.06\bin;D:\RCompile\CRANpkg\extralibs\Qt\bin;D:\RCompile\CRANpkg\extralibs\Qt\qt\bin;D:\RCompile\CRANpkg\extralibs\merlin;d:\compiler\xpdf;d:\compiler\qpdf\bin;d:\Compiler\Aspell\bin;d:\Compiler\python

set R_PARALLEL_PORT=random
set R_GC_GROWINCRFRAC=0.2

set LC_COLLATE=C
set LC_MONETARY=C
set LC_TIME=C
set R_INSTALL_TAR=tar.exe
set R_GSCMD=C:\Progra~2\gs\gs9.06\bin\gswin32c.exe
set R_BROWSER=false

set OMP_THREAD_LIMIT=4


set CYGWIN=nodosfilewarning
set TAR_OPTIONS=--no-same-owner --no-same-permissions

set LANGUAGE=en
set TMPDIR=d:\temp
set _R_CHECK_COMPACT_DATA2_=TRUE
set _R_CHECK_EXECUTABLES_=FALSE
set _R_CHECK_EXECUTABLES_EXCLUSIONS_=FALSE
set _R_CHECK_FORCE_SUGGESTS_=FALSE
set _R_CHECK_HAVE_ODBC_=TRUE
set _R_CHECK_HAVE_MYSQL_=TRUE
set _R_CHECK_HAVE_POSTGRES_=TRUE
set _R_CHECK_HAVE_PERL_=TRUE
set _R_CHECK_INSTALL_DEPENDS_=TRUE
set _R_CHECK_LICENSE_=FALSE
set _R_CHECK_NO_RECOMMENDED_=TRUE
set _R_CHECK_SUGGESTS_ONLY_=TRUE
set _R_SHLIB_BUILD_OBJECTS_SYMBOL_TABLES_=TRUE
set _R_CHECK_WINDOWS_DEVICE_=TRUE
set _R_CHECK_SCREEN_DEVICE_=warn
set _R_CHECK_LIMIT_CORES_=true

set POSTGRES_USER=postgres
set POSTGRES_PASSWD=DatenBank321
set POSTGRES_HOST=localhost
set POSTGRES_DATABASE=postgresTest
set MYSQL_USER=CRAN
set MYSQL_PASSWD=DatenBank321
set MYSQL_HOST=localhost
set MYSQL_DATABASE=test
set ODBC_USER=CRAN
set ODBC_PASSWD=DatenBank321
set ODBC_HOST=localhost
set ODBC_DATABASE=test
